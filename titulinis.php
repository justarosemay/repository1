<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Atlikėjas tavo mieste</title>


	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<link rel="stylesheet" type="text/css" href="css/titulinis.css">
	
	

</head>

<body bgcolor="#E6E6FA">


	<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "projektas";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if ($_GET != null) {
	$sql = "INSERT INTO prenumeratoriai (ID, email)
VALUES (null, '" . $_GET["email"] . "')";

} 

// if (mysqli_query($conn, $sql)) {
// 	br;
// }
    // echo "New record created successfully";
// } else {
//     echo "Error: " . $sql . "<br>" . mysqli_error($conn);
// }


// echo "Connected successfully";

?>


		<ul id="dropdown1" class="dropdown-content">
		  <li><a href="MayMyPartList2.html">Viktorija</a></li>
		  <li><a href="MayMyPartList3.html">Maja</a></li>
		  <!-- <li class="divider"></li> -->
		  <li><a href="MayMyPartList4.html">Justas</a></li>
		</ul>

		<nav>
		  <div class="nav-wrapper">
		    <a href="titulinis.php" class="brand-logo"><img src="images/microphonelogo.png"></a>

		    <ul class="right hide-on-med-and-down">
		      <li><a href="index2pg.html">Naujienos</a></li>
		      <li><a href="index2pg.html">Atlikėjų sąrašas</a></li>
		      <!-- Dropdown Trigger -->
		      <li><a class="dropdown-button" href="#" data-activates="dropdown1">Apie mus<i class="material-icons right">arrow_drop_down</i></a></li>
		    </ul>
		  </div>
		</nav>
		
		<nav class="nav-breadcrumb">
    		<div class="nav-wrapper2">
      			<div class="col s12">
        <a href="titulinis.php" class="breadcrumb"><b>Titulinis</b></a>
        <a href="titulinis.php" class="breadcrumb">...</a>        
     			</div>
   			</div>

  		</nav>


	<div class="header-photo col s12">

		<h1 class="header-title">Labiausiai tinkantys atlikėjai Jūsų renginiui</h1>
		<a href="index2pg.html" class="waves-effect waves-light btn-large"><i class="material-icons left">queue_music</i>VISI ATLIKĖJAI</a>

	</div>


	<div class="content-wrapper">
		<h2>Atlikėjų TOP3</h2>
		<ul class="artists">
			<li>
				<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="images/mamontovas.jpg" alt="mamontovas">
							<!-- front content -->
						</div>
						<div class="back">
							<p><b>Kaina: 200 €</b></p>
							<!-- back content -->
						</div>
					</div>
				</div>

				<p class="name"><b>Andrius Mamontovas</b></p>
				<p class="description">Grupės Foje įkūrėjas ir vienintelis pastovus narys (1983–1997), šiuo metu – solo muzikantas, aktorius, fotografas ir prodiuseris (dalyvavo įrašant visus grupės „Foje“ albumus), prodiusavo grupės „Mano Juodoji Sesuo“ albumą „Bellamy“ (1996 m.), pirmus du „Sel“ albumus, „Naktinių personų“ „Muzika ir Daugiau“, Jurgos Šeduikytės „Aukso pievą“ bei daugybę kitų albumų.</p>
				<p></p>
				<iframe width="200" height="200" src="https://www.youtube.com/embed/MiSqj3bIe-0?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				<p></p>

				<a href="MayMyPartList.html" class="waves-effect waves-light btn-large"><i class="material-icons left">queue_music</i>Daugiau</a>

				<!-- <p></p>
				<p>Andriaus Mamontovo vaizdo klipą galite rasti <a href="https://www.youtube.com/watch?v=MiSqj3bIe-0" target="_blank">ČIA.</a></p> -->
				
			</li><!--  
			 --><li>
			 	<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="images/jazzu.jpg" alt="">
							<!-- front content -->
						</div>
						<div class="back">
							<p><b>Kaina: 400 €</b></p>
							<!-- back content -->
						</div>
					</div>
				</div>
				
				<p class="name"><b>JAZZU</b></p>
				<p class="description">Justė Arlauskaitė-Jazzu pradėjo dainuoti džiazą nuo 13 metų. Kaip solistė ji pasirodydavo su grupėmis ir įvairiais džiazo projektais Lietuvoje ir užsienyje. 15-kos metų ji tapo garsios Lietuvos elektroninės grupės „Pieno lazeriai“ (dar žinomos kaip „Milky lasers“) vokaliste. Kaip solistė Justė Arlauskaitė-Jazzu pasirodydavo su grupėmis ir įvairiais džiazo projektais Lietuvoje ir užsienyje. </p>
				<p></p>
				<iframe width="200" height="200" src="https://www.youtube.com/embed/MllUwWlcOxE?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

				<p></p>

				<a href="MayMyPartList.html" class="waves-effect waves-light btn-large"><i class="material-icons left">queue_music</i>Daugiau</a>
				
			</li><!-- 
			 --><li>
			 	<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
					<div class="flipper">
						<div class="front">
							<img src="images/sindikatas.jpg" alt="">
							<!-- front content -->
						</div>
						<div class="back">
							<p><b>Kaina: 300 €</b></p>
							<!-- back content -->
						</div>
					</div>
				</div>
				
				<p class="name"><b>G&G Sindikatas</b></p>
				<p class="description">Grupė tepasižymejo tuo, kad sudalyvavo muštynėmis pasibaigusiame Vievio alternatyvios muzikos festivalyje.Svaras nusifotografavo kultiniam to meto žurnale Moksleivis ir tuo metu susitiko su būsimaisiais grupės nariais Kastyčiu Sarnicku (Kastetu) ir Donatu Juršėnu (Donciavu).Apie 1996 metus Svaras su bičiuliu Giga įkūrė grupę pavadinimu G&G.</p>
				<p></p>
				<iframe width="200" height="200" src="https://www.youtube.com/embed/RS1T9aZHn5c?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

				<p></p>

				<a href="MayMyPartList.html" class="waves-effect waves-light btn-large"><i class="material-icons left">queue_music</i>Daugiau</a>
				
			</li>

		</ul>
	</div>

 <footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Atlikėjas tavo mieste</h5>
                <p class="grey-text text-lighten-4">Pasiūlysime labiausiai tinkančius atlikėjus Jūsų šventei ar renginiui</p>

            <form action="">
                 <div class="row">
		    		<div class="input-field col s12">
		      			<input value="@" id="first_name2" name="email" type="text" class="validate">
		      			<label class="active" for="first_name2"><b>NAUJIENLAIŠKIO PRENUMERATA</b></label>
		    </form>

	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9634.177819884542!2d-1.2123896138508776!3d52.86660822809827!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4879e7fdc238d091%3A0x1c80d387bdf87dd5!2sGotham%2C+Notingamas%2C+Jungtin%C4%97+Karalyst%C4%97!5e0!3m2!1slt!2slt!4v1508520539320" width="400" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
		    </div>
  		</div>

              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Mus galite rasti:</h5>
                <ul>
                  <li><img src="images/youtube.png"><a class="grey-text text-lighten-3" href="https://www.youtube.com/watch?v=MiSqj3bIe-0" target="_blank"> Youtube</a></li>                  
                  <li><img src="images/facebook.svg"><a class="grey-text text-lighten-3" href="https://www.facebook.com/linkinpark/" target="_blank"> Facebook</a></li>
                  <li><img src="images/instagram.png"><a class="grey-text text-lighten-3" href="https://www.instagram.com/linkinpark/" target="_blank"> Instagram</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2017 visos teisės saugomos            
            </div>
          </div>
    </footer>
            
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="js/titulinis.js" type="text/javascript"></script>

</body>
</html>